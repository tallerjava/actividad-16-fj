mysql -u root

CREATE DATABASE BaseLocal;

USE BaseLocal;

GRANT ALL PRIVILEGES ON BaseLocal.* TO 'usuario'@localhost IDENTIFIED BY 'caperusita20';

CREATE TABLE COTIZACION_HISTORICO (
	Cotizacion_ID MEDIUMINT NOT NULL AUTO_INCREMENT,
	Moneda CHAR(3) NOT NULL,
	Precio DOUBLE NOT NULL,
	Proveedor VARCHAR(15) NOT NULL,
	Fecha DATETIME NOT NULL,
	PRIMARY KEY (Cotizacion_ID)
);

INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2018-01-26 10:09:03", 'USD', 1000.01,'Binance');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2018-02-26 11:00:35", 'USD', 1200.01,'CoinDesk');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2018-03-26 22:30:46", 'USD', 1300.01,'CryptoCompare');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2018-04-26 13:16:36", 'USD', 1400.01,'SomosPNT');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2018-05-26 09:02:14", 'USD', 8000.51,'FinanSur');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2017-06-26 10:44:21", 'USD', 8000.51,'Binance');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2017-07-25 13:40:30", 'USD', 8000.51,'CoinDesk');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2017-07-24 18:00:59", 'USD', 8000.07,'CryptoCompare');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2017-07-23 07:50:37", 'USD', 7999.22,'SomosPNT');
INSERT INTO COTIZACION_HISTORICO (Fecha, Moneda, Precio, Proveedor) VALUES ("2017-07-22 11:00:21", 'USD', 7581.15,'FinanSur');