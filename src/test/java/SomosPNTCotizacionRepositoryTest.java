
import com.cotizacionweb.cotizacion.Cotizacion;
import com.cotizacionweb.repository.SomosPNTCotizacionRepository;
import org.json.JSONException;
import org.junit.Test;
import static org.junit.Assert.*;

public class SomosPNTCotizacionRepositoryTest {

    public SomosPNTCotizacionRepositoryTest() {
    }

    @Test
    public void obtenerCotizacion_sinUrl_cotizacionNullObtenida() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl("");
        Cotizacion resultadoObtenido = somosPNTCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null;
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test(expected = JSONException.class)
    public void obtenerCotizacion_urlInvalida_JSONException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl("https://api.coindesk.com/v1/bpi/currentprice.xml");
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_urlNull_NullPointerException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl(null);
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionNullObtenida() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        Cotizacion resultadoObtenido = somosPNTCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null;
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
