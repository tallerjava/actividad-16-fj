
import com.cotizacionweb.cotizacion.Cotizacion;
import com.cotizacionweb.repository.FinanSurCotizacionRepository;
import org.junit.Test;
import static org.junit.Assert.*;

public class FinanSurCotizacionRepositoryTest {

    public FinanSurCotizacionRepositoryTest() {
    }

    public void obtenerCotizacion_urlInvalida_cotizacionNull() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal("jdbc:mysql://localhost:3306/OtraBaseLocal");
        Cotizacion resultadoObtenido = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null;
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionObtenida() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        assertNotNull(finanSurCotizacionRepository.obtenerCotizacion());
    }

    @Test
    public void obtenerCotizacion_urlNull_cotizacionNull() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal(null);
        Cotizacion resultadoObtenido = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null;
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void obtenerCotizacion_usuarioInvalido_cotizacionNull() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUsuario("otroUser");
        Cotizacion resultadoObtenido = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null;
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void obtenerCotizacion_passwordInvalido_cotizacionNull() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setPassword("nuevaContr4");
        Cotizacion resultadoObtenido = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null;
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
