<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mis Cotizaciones</title>
    </head>
    <body>        
    <center><h1>Presione "Refrescar" para obtener las ultimas cotizaciones</h1></center>
        <c:forEach items="${requestScope.cotizacionListado}" var="item">
            <c:choose>
                <c:when test="${item.getPrecio() > -1}">
                    <center>${item}</center><br>
                </c:when>
                <c:otherwise>
                    <center>${item.getNombreProveedor()}: (El proveedor no se encuentra disponible)</center><br>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <center><a href="PruebaServlet">Refrescar</a></center>
</body>
</html>