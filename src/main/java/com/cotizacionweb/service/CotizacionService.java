package com.cotizacionweb.service;

import com.cotizacionweb.cotizacion.Cotizacion;
import com.cotizacionweb.repository.CotizacionRepository;
import com.cotizacionweb.repository.GestorCotizacionRepository;
import java.util.ArrayList;

public class CotizacionService {

    private ArrayList<CotizacionRepository> cotizacionRepositoryListado;

    public CotizacionService(ArrayList<CotizacionRepository> cotizacionRepositoryListado) {
        this.cotizacionRepositoryListado = cotizacionRepositoryListado;
    }

    public ArrayList<Cotizacion> obtenerListadoCotizacion() {
        ArrayList<Cotizacion> cotizacionListado = new ArrayList();
        GestorCotizacionRepository gestorCotizacionRepository = new GestorCotizacionRepository();
        for (CotizacionRepository cotizacionRepository : cotizacionRepositoryListado) {
            String nombreProveedor = cotizacionRepository.getNombreProveedor();
            Cotizacion cotizacion;
            try {
                cotizacion = cotizacionRepository.obtenerCotizacion();
            } catch (Exception obtenerCotizacionException) {
                cotizacion = null;
            }
            if (cotizacion != null) {
                cotizacionListado.add(cotizacion);
                try {
                    gestorCotizacionRepository.guardarCotizacion(cotizacion);
                } catch (Exception guardarCotizacionException) {
                    guardarCotizacionException.printStackTrace();
                }
            } else {
                try {
                    cotizacionListado.add(gestorCotizacionRepository.obtenerUltimaCotizacion(nombreProveedor));
                } catch (Exception obtenerUltimaCotizacionException) {
                    cotizacionListado.add(new Cotizacion(nombreProveedor, null, null, -1));
                }
            }
        }
        return cotizacionListado;
    }
}
