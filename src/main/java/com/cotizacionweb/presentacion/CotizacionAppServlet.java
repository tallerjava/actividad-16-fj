package com.cotizacionweb.presentacion;

import com.cotizacionweb.cotizacion.Cotizacion;
import com.cotizacionweb.repository.*;
import com.cotizacionweb.service.CotizacionService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "PresentacionCotizacion", urlPatterns = {"/PresentacionCotizacion"})
public class CotizacionAppServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<CotizacionRepository> cotizacionRepositoryListado = new ArrayList();
        cotizacionRepositoryListado.add(new CoinDeskCotizacionRepository());
        cotizacionRepositoryListado.add(new BinanceCotizacionRepository());
        cotizacionRepositoryListado.add(new CryptoCompareCotizacionRepository());
        cotizacionRepositoryListado.add(new FinanSurCotizacionRepository());
        cotizacionRepositoryListado.add(new SomosPNTCotizacionRepository());
        CotizacionService cotizacionService = new CotizacionService(cotizacionRepositoryListado);
        ArrayList<Cotizacion> cotizacionListado = cotizacionService.obtenerListadoCotizacion();
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Mis Cotizaciones</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center><h1>Presione Enter para refrescar</h1></center>");
            out.println("<form action=\"PresentacionCotizacion\" method=\"GET\"> ");
            out.println("<center><input type=\"submit\" value=\"Enter\"></center>");
            out.println("</form>");
            for (Cotizacion cotizacion : cotizacionListado) {
                if (cotizacion.getPrecio() > -1) {
                    out.println("<center><br>" + cotizacion + "</br></center>");
                } else {
                    out.println("<center><br>" + cotizacion.getNombreProveedor() + ":\t(El proveedor no se encuentra disponible) </br></center>");
                }
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold> 
}
