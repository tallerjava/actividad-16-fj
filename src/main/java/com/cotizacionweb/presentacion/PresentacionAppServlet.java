
package com.cotizacionweb.presentacion;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "PresentacionInicial", urlPatterns = {"/PresentacionInicial"})
public class PresentacionAppServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Mis Cotizaciones</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<center><h1>Presione Enter para obtener la ultima cotizacion del dia</h1></center>");
            out.println("<form action=\"PresentacionCotizacion\" method=\"GET\"> ");
            out.println("<center><input type=\"submit\" value=\"Enter\"></center>");
            out.println("</form>"); 
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
