
package com.cotizacionweb.presentacion;

import com.cotizacionweb.cotizacion.Cotizacion;
import com.cotizacionweb.repository.BinanceCotizacionRepository;
import com.cotizacionweb.repository.CoinDeskCotizacionRepository;
import com.cotizacionweb.repository.CotizacionRepository;
import com.cotizacionweb.repository.CryptoCompareCotizacionRepository;
import com.cotizacionweb.repository.FinanSurCotizacionRepository;
import com.cotizacionweb.repository.SomosPNTCotizacionRepository;
import com.cotizacionweb.service.CotizacionService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "PruebaServlet", urlPatterns = {"/PruebaServlet"})
public class PruebaServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<CotizacionRepository> cotizacionRepositoryListado = new ArrayList();
        cotizacionRepositoryListado.add(new CoinDeskCotizacionRepository());
        cotizacionRepositoryListado.add(new BinanceCotizacionRepository());
        cotizacionRepositoryListado.add(new CryptoCompareCotizacionRepository());
        cotizacionRepositoryListado.add(new FinanSurCotizacionRepository());
        cotizacionRepositoryListado.add(new SomosPNTCotizacionRepository());
        CotizacionService cotizacionService = new CotizacionService(cotizacionRepositoryListado);
        ArrayList<Cotizacion> cotizacionListado = cotizacionService.obtenerListadoCotizacion();
                
        request.setAttribute("cotizacionListado", cotizacionListado);
        request.getRequestDispatcher("/prueba.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
