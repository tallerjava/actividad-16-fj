package com.cotizacionweb.repository;

import com.cotizacionweb.cotizacion.Cotizacion;
import java.util.Date;
import jodd.http.HttpException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class SomosPNTCotizacionRepository extends CotizacionRepository {

    private String url = "http://dev.somospnt.com:9756/quote";
    private String nombreProveedor = "SomosPNT";
    private String moneda = "USD";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }
    
    @Override
    public String getMoneda() {
        return moneda;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        Cotizacion cotizacion = null;
        try {
            HttpResponse response = HttpRequest.get(url).send();
            JSONObject body = new JSONObject(response.body());
            Date fecha = new Date();
            double precio = body.getDouble("price");
            cotizacion = new Cotizacion(nombreProveedor, fecha, moneda, precio);
        } catch (HttpException responseException){
            responseException.printStackTrace();
        }
        return cotizacion;
    }
}
