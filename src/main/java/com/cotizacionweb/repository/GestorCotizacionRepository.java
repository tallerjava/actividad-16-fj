package com.cotizacionweb.repository;

import com.cotizacionweb.cotizacion.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class GestorCotizacionRepository {

    private String urlLocal = "jdbc:mysql://localhost:3306/BaseLocal";
    private String usuario = "usuario";
    private String password = "caperusita20";

    public GestorCotizacionRepository() {
    }

    public String getUrlLocal() {
        return urlLocal;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUrlLocal(String urlLocal) {
        this.urlLocal = urlLocal;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Cotizacion obtenerUltimaCotizacion(String nombreProveedor) throws SQLException {
        Cotizacion cotizacion = null;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (Exception registerDriverManagerException) {
            registerDriverManagerException.printStackTrace();
        }
        try (Connection conexion = DriverManager.getConnection(urlLocal, usuario, password); Statement estado = conexion.createStatement()) {
            ResultSet resultado = estado.executeQuery("select * from cotizacion_historico where proveedor = '" + nombreProveedor + "' and fecha <= NOW() order by fecha desc limit 0,1;");
            while (resultado.next()) {
                cotizacion = new Cotizacion(nombreProveedor, resultado.getTimestamp("fecha"), resultado.getString("moneda"), resultado.getDouble("precio"));
            }
            estado.close();
            conexion.close();
        } catch (Exception obtenerUltimaCotizacionException) {
            throw new SQLException("obtenerUltimaCotizacionException: " + obtenerUltimaCotizacionException.getMessage());
        }
        return cotizacion;
    }

    public void guardarCotizacion(Cotizacion cotizacion) throws SQLException {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (Exception registerDriverManagerException) {
            registerDriverManagerException.printStackTrace();
        }
        try (Connection conexion = DriverManager.getConnection(urlLocal, usuario, password)) {
            if (!cotizacion.getNombreProveedor().equals("FinanSur")) {
                String query = "insert into cotizacion_historico (proveedor, fecha, moneda, precio) values (?, ?, ?, ?)";
                PreparedStatement estado = conexion.prepareStatement(query);
                estado.setString(1, cotizacion.getNombreProveedor());
                estado.setString(2, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cotizacion.getFecha()));
                estado.setString(3, cotizacion.getMoneda());
                estado.setString(4, Double.toString(cotizacion.getPrecio()));
                estado.executeUpdate();
                estado.close();
            }
            conexion.close();
        } catch (Exception guardarCotizacionException) {
            throw new SQLException("guardarCotizacionException: " + guardarCotizacionException.getMessage());
        }
    }
}
