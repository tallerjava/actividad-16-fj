
package com.cotizacionweb.repository;

import com.cotizacionweb.cotizacion.Cotizacion;

public abstract class CotizacionRepository {

    public abstract String getNombreProveedor();

    public abstract Cotizacion obtenerCotizacion();
    
    public abstract String getMoneda();
}